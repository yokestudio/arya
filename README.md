# Arya #

A JavaScript library of functions meant for Arrays of objects.

Useful for implementing databases

### Browser Support ###
* IE 8+ (even older browsers should actually be ok, but since nobody uses those anymore, we won't track them)
* Chrome
* Edge
* Firefox
* Opera
* Safari

### Set Up ###

1. Download the latest release (as of 6 Apr 2019, v1.1.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="arya-1.1.0.min.js"></script>`
    
3. You may also consider including polyfills to augment your native arrays [ES-6 Array](https://gitlab.com/yokestudio/es6-array-polyfill)

### API ###
**`mergeBy`**

```
/**
 * Merges arrays of objects by a property, e.g. id
 * - Note: objects without the property will not be included in results
 * - Note: replaces objects with same id with the latest object
 * @param: {string} prop
 *         {Array, Array*} arrays of objects
 * @return: {Array} - new array; original arrays are not touched
 */

// Sample usage
var old_db = [
    {id: 1, name:'John'},
	{id: 2, name:'Jane'}
];
var updated = [
    {id: 2, name:'Jenny'},
	{id: 3, name:'Newton'}
];
var persons = arya.mergeBy('id', old_db, updated);
// persons == [{id: 1, name:'John'}, {id: 2, name:'Jenny'}, {id: 3, name:'Newton'}]
```

**`sortBy`**

```
/**
 * Sorts an array of objects by a certain prop.
 * - Note: sort is ALWAYS stable
 * - Note: new array will be returned; original arrays are untouched
 * @param: {string} prop
 *         {Array} array of objects
 *         {object} options
 *             {string} order - case insensitive (default:'asc') ['asc', 'desc']
 *             {function} primer (default: null)
 * @return: {Array}
 */
 
// Sample usage
var unsorted = [
	{id: 3, name:'Newton'},
	{id: 1, name:'John'},
	{id: 2, name:'Jane'}
];
var sorted = arya.sortBy('id', unsorted);
// sorted == [{id: 1, name:'John'}, {id: 2, name:'Jenny'}, {id: 3, name:'Newton'}]
```

**`count`**

```
/**
 * Counts the occurrences of objects containing certain key-value pairs in the array
 * @param: {object} conditions - key-value pairs
 *         {Array} array - of objects
 *         {object} options
 *             {boolean} strict: whether to use === or ==
 * @return: {int}
 */

// Sample usage
var persons = [
    {name:'John', age: 32},
	{name:'Jane', age: 21},
	{name:'Newton', age: 32}
];
var count = arya.count({age: 32}, persons); // returns 2
```

**`indexOf`**

```
/**
 * Returns index of the first object in the array that satisifies the conditions given
 * - Note: short-circuit return on match, so integrity of the rest of the Array is not checked.
 * @param: {object} conditions - key-value pairs
 *         {Array} array - of objects
 *         {object} options
 *             {boolean} strict: whether to use === or ==
 * @return: {int}
 */

// Sample usage
var persons = [
    {name:'John', age: 32},
	{name:'Jane', age: 21},
	{name:'Newton', age: 32}
];
var index = arya.indexOf({age: 21}, persons); // returns 1
```

**`findByMax / findByMin`**

```
/**
 * Finds the object with the max/min value for a particular property
 * - Note: objects without prop specified are ignored
 * - Note: props are compared using relational operators, so it's best to keep type same, or use a primer (parseInt)
 * @param: {string} prop
 *         {Array} array of objects
 *         {object} options
 *             {function} primer (default: null)
 * @return: {object} - null if not found
 */

// Sample usage
var persons = [
    {name:'John', age: 32},
	{name:'Jane', age: 21},
	{name:'Newton', age: 32}
];
var oldest_person = arya.findByMax('age', persons);  // {name:'John', age: 32}
var youngest_person = arya.findByMin('age', persons);  // {name:'Jane', age: 21}
```

**`extract`**
```
/**
 * Extracts a particular property value from an array of objects
 * @param {string} prop
 * @param {Array} array - of objects
 * @return {Array}
 */

// Sample usage
var persons = [
    {name:'John', age: 32},
	{name:'Jane', age: 21},
	{name:'Newton', age: 32}
];
var names = arya.extract('name', persons);  // ['John', 'Jane', 'Newton']
```

### FAQ ###

1. Do the functions work with Array-like collections?
> No. It only works with Arrays. Convert all Array-like collections into Arrays before use:
> `var array = [].slice.call(arrayLikeCollection);`
> or
> `let array = Array.from(arrayLikeCollection); // requires ES-6` 


1. Why is there no method to find all objects with a certain property greater/less than a certain value?
> These types of filters can be easily created using built-in `Array.prototype.filter()` method.

### Contact ###

* Email us at <yokestudio@hotmail.com>.