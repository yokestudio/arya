/***
 * Library - Arya
 *
 * Useful methods for Arrays of Objects
 ***/
(function(global) {
	'use strict';

/*************
 *  Generic  *
 *************/
	/**
	 * Merges arrays of objects by a property, e.g. id
	 * - Note: does not mutate any Array args
	 * - Note: objects without the given property will not be included in results
	 * - Note: replaces objects with same id with the latest object
	 * @param {string} prop
	 * @param: {Array, Array, ...Array} arrays of objects
	 * @return {Array} - new array
	 */
	function mergeBy(prop) {
		var i, j, k, len_i, len_j, len_k, replaced; // loop vars

		var len = arguments.length;

		// Check params
		if (len < 3) { // eslint-disable-line no-magic-numbers
			throw new TypeError('mergeBy() - expects at least 3 args');
		}
		if (typeof arguments[0] !== 'string') {
			throw new TypeError('mergeBy() - expect string argument for prop, got ' + typeof arguments[0]);
		}
		for (i = 1, len_i = arguments.length; i < len_i; i++) {
			if (typeof arguments[i] !== 'object' || !(arguments[i] instanceof Array)) {
				throw new TypeError('mergeBy() - expect Array arguments, got ' + typeof arguments[i]);
			}
		}

		var result = [];

		for (i = 1, len_i = arguments.length; i < len_i; i++) {
			var a = arguments[i];

			// Go through each object
			for (j = 0, len_j = a.length; j < len_j; j++) {
				if (typeof a[j] !== 'object') {
					throw new TypeError('mergeBy() - expect argument[' + i + '] to be Array of objects, got ' + typeof a[j]);
				}
				if (a[j] === null) {
					throw new TypeError('mergeBy() - expect argument[' + i + '] to be Array of objects, got null.');
				}
				if (typeof a[j][prop] === 'undefined') {
					continue;
				}

				// try to replace in the results array
				replaced = false;
				for (k = 0, len_k = result.length; k < len_k; k++) {
					if (result[k][prop] === a[j][prop]) {
						result[k] = a[j];
						replaced = true;
						break;
					}
				}

				// add new member at the end
				if (!replaced) {
					result.push(a[j]);
				}
			} // for each object
		} // for each array

		return result;
	} // mergeBy()

	/**
	 * Sorts an array of objects by a certain prop, e.g. id
	 * - Note: sort is ALWAYS stable
	 * - Note: new array will be returned; original arrays are untouched
	 * @param {string} prop
	 *        {Array} array of objects
	 *        {object=} opts
	 *             {string} order - case insensitive (default:'asc') ['asc', 'desc']
	 *             {function} primer (default: null)
	 * @return: {Array}
	 */
	function sortBy(prop, array, opts) {
		var i, len; // loop vars

		if (typeof prop !== 'string') {
			throw new TypeError('sortBy() - expect string argument for prop, got ' + typeof prop);
		}
		if (!(array instanceof Array)) {
			throw new TypeError('sortBy() - expect Array argument for array, got ' + typeof array);
		}

		var options = Object.assign({'order': 'asc', 'primer': null}, opts);
		if (typeof options['order'] === 'string') {
			options['order'] = options['order'].toLowerCase();
		}
		if (typeof options['order'] !== 'string' || options['order'].substr(0, 4) !== 'desc') { // eslint-disable-line no-magic-numbers
			options['order'] = 'asc';
		}
		if (typeof options['primer'] !== 'function') {
			options['primer'] = null;
		}

		// create new array that remembers the object's original starting positions
		var arrayWithIndices = [];
		for (i = 0, len = array.length; i < len; i++) {
			if (typeof array[i] !== 'object') {
				throw new TypeError('sortBy() - expects Array of objects. Found ' + typeof array[i]);
			}
			arrayWithIndices.push({
				'elem': array[i],
				'pos': i
			});
		}

		arrayWithIndices.sort(_arraySortBy(prop, options['order'] === 'asc', options['primer']));

		// retrieve elements from sorted arrayWithIndices
		var returnArray = [];
		for (i = 0, len = array.length; i < len; i++) {
			returnArray.push(arrayWithIndices[i]['elem']);
		}

		return returnArray;
	} // sortBy

	/**
	 * @private
	 * helper function for sortBy() implementing stable sort
	 */
	function _arraySortBy(prop, asc_order, primer) {
		var getValue = primer ?
			function(x) {
				return primer(x['elem'][prop]);
			} :
			function(x) {
				return x['elem'][prop];
			};

		return function(a, b) {
			var val_a = getValue(a);
			var val_b = getValue(b);

			if (val_a > val_b) {
				return asc_order ? 1 : -1;
			}
			if (val_b > val_a) {
				return asc_order ? -1 : 1;
			}
			if (a['pos'] > b['pos']) {
				return asc_order ? 1 : -1;
			}
			if (b['pos'] > a['pos']) {
				return asc_order ? -1 : 1;
			}

			throw new RangeError('WTF: ' + b['pos'] + ', ' + a['pos']);

			// return asc_order * ((a > b) - (b > a));
		};
	} // _arraySortBy()

	/**
	 * Counts the occurrences of objects containing certain key-value pairs in the array
	 * @param {Object} conditions - key-value pairs
	 *        {Array} array - of objects
	 *        {Object=} opts
	 *             {boolean} strict: whether to use === or ==
	 * @return {int}
	 */
	function count(conditions, array, opts) {
		var i, len; // loop vars

		if (typeof conditions !== 'object') {
			throw new TypeError('count() - expect object argument for conditions, got ' + typeof conditions);
		}
		if (!(array instanceof Array)) {
			throw new TypeError('count() - expect Array argument for array, got ' + typeof array);
		}

		var options = Object.assign({'strict': true}, opts);
		if (typeof options['strict'] !== 'boolean') {
			options['strict'] = true;
		}

		// Go through the array
		var counter = 0;
		for (i = 0, len = array.length; i < len; i++) {
			var obj = array[i];
			if (typeof obj !== 'object') {
				throw new TypeError('count() - expects Array of objects. Found ' + typeof obj);
			}

			if (satisfy(conditions, obj, options['strict'])) {
				counter++;
			}
		} // for each object in array

		return counter;
	} // count()

	/**
	 * Returns index of the first object in the array that satisifies the conditions given
	 * - Note: short-circuit return on match, so integrity of the rest of the Array is not checked.
	 * @param: {object} conditions - key-value pairs, e.g. {id: 4, enabled: true}
	 *         {Array} array - of objects
	 *         {Object=} opts
	 *             {boolean} strict: whether to use === or == (default: true)
	 * @return: {int}
	 */
	function indexOf(conditions, array, opts) {
		var i, len; // loop vars

		if (typeof conditions !== 'object') {
			throw new TypeError('indexOf() - expect object argument for conditions, got ' + typeof conditions);
		}
		if (!(array instanceof Array)) {
			throw new TypeError('indexOf() - expect Array argument for array, got ' + typeof array);
		}

		var options = Object.assign({'strict': true}, opts);
		if (typeof options['strict'] !== 'boolean') {
			options['strict'] = true;
		}

		// Go through the array
		for (i = 0, len = array.length; i < len; i++) {
			var obj = array[i];
			if (typeof obj !== 'object') {
				throw new TypeError('indexOf() - expects Array of objects. Found ' + typeof obj);
			}

			if (satisfy(conditions, obj, options['strict'])) {
				return i;
			}
		} // for each object in array

		return -1;
	} // indexOf()

	/**
	 * @private
	 * helper function to check if an object contains key-value pairs specified in conditions
	 */
	function satisfy(conditions, obj, useStrict) {
		for (var prop in conditions) {
			if (!conditions.hasOwnProperty(prop)) {
				continue;
			}

			// Use specified compare operator
			if (useStrict) {
				if (obj[prop] !== conditions[prop]) {
					return false;
				}
			} // strict
			else {
				if (obj[prop] != conditions[prop]) { // eslint-disable-line eqeqeq
					return false;
				}
			} // non-strict
		}
		return true;
	} // satisfy()

	/**
	 * Finds the object with the max value for a particular property
	 * - Note: objects without prop specified are ignored
	 * - Note: props are compared using relational operators, so it's best to keep type same, or use a primer (parseInt)
	 * @param {string} prop
	 *        {Array} array of objects
	 *        {Object=} opts
	 *            {function} primer (default: null)
	 * @return {object} - null if not found
	 */
	function findByMax(prop, array, opts) {
		var i, len; // loop vars

		if (typeof prop !== 'string') {
			throw new TypeError('findByMax() - expect string argument for prop, got ' + typeof prop);
		}
		if (!(array instanceof Array)) {
			throw new TypeError('findByMax() - expect Array argument for array, got ' + typeof array);
		}

		var options = Object.assign({'primer': null}, opts);
		if (typeof options['primer'] !== 'function') {
			options['primer'] = null;
		}

		// Edge case
		len = array.length;
		if (len === 0) {
			return null;
		}

		// Assume first element is max
		var max_obj = array[0];
		if (typeof max_obj !== 'object') {
			throw new TypeError('findByMax() - expects Array of objects. Found ' + typeof max_obj);
		}
		if (typeof max_obj[prop] === 'undefined') {
			max_obj = null;
		}

		// Go through the array
		for (i = 1; i < len; i++) {
			var obj = array[i];
			if (typeof obj !== 'object') {
				throw new TypeError('findByMax() - expects Array of objects. Found ' + typeof obj);
			}

			// Ignore objects without prop specified
			if (typeof obj[prop] === 'undefined') {
				continue;
			}

			// Compare
			if (options['primer']) {
				if (options['primer'](obj[prop]) > options['primer'](max_obj[prop])) {
					max_obj = obj;
				}
			}
			else {
				if (obj[prop] > max_obj[prop]) {
					max_obj = obj;
				}
			}
		} // for each object in array

		return max_obj;
	} // findByMax()

	/**
	 * Finds the object with the min value for a particular property
	 * - Note: objects without prop specified are ignored
	 * - Note: props are compared using relational operators, so it's best to keep type same, or use a primer (parseInt)
	 * @param: {string} prop
	 *         {Array} array of objects
	 *         {object} opts
	 *             {function} primer (default: null)
	 * @return: {object} - null if not found
	 */
	function findByMin(prop, array, opts) {
		var i, len; // loop vars

		if (typeof prop !== 'string') {
			throw new TypeError('findByMin() - expect string argument for prop, got ' + typeof prop);
		}
		if (!(array instanceof Array)) {
			throw new TypeError('findByMin() - expect Array argument for array, got ' + typeof array);
		}

		var options = Object.assign({'primer': null}, opts);
		if (typeof options['primer'] !== 'function') {
			options['primer'] = null;
		}

		// Edge case
		len = array.length;
		if (len === 0) {
			return null;
		}

		// Assume first element is min
		var min_obj = array[0];
		if (typeof min_obj !== 'object') {
			throw new TypeError('findByMin() - expects Array of objects. Found ' + typeof min_obj);
		}
		if (typeof min_obj[prop] === 'undefined') {
			min_obj = null;
		}

		// Go through the array
		for (i = 1; i < len; i++) {
			var obj = array[i];
			if (typeof obj !== 'object') {
				throw new TypeError('findByMin() - expects Array of objects. Found ' + typeof obj);
			}

			// Ignore objects without prop specified
			if (typeof obj[prop] === 'undefined') {
				continue;
			}

			// Compare
			if (options['primer']) {
				if (options['primer'](obj[prop]) < options['primer'](min_obj[prop])) {
					min_obj = obj;
				}
			}
			else {
				if (obj[prop] < min_obj[prop]) {
					min_obj = obj;
				}
			}
		} // for each object in array

		return min_obj;
	} // findByMin()

	/**
	 * Extracts a particular property value from an array of objects
	 * @param {string} prop
	 * @param {Array} array - of objects
	 * @return {Array}
	 */
	function extract(prop, array) {
		if (typeof prop !== 'string') {
			throw new TypeError('extract() - expect string argument for prop, got ' + typeof prop);
		}
		if (!(array instanceof Array)) {
			throw new TypeError('extract() - expect Array argument for array, got ' + typeof array);
		}

		return array.map(function(obj, i) {
			if (typeof obj !== 'object' || obj === null) {
				throw new TypeError('extract() - expect Array of objects. Found: ' + obj + ' at index ' + i);
			}
			return obj[prop];
		});
	} // extract()

	global['arya'] = {
		'mergeBy': mergeBy,
		'sortBy': sortBy,
		'count': count,
		'indexOf': indexOf,
		'findByMax': findByMax,
		'findMax': findByMax,
		'findByMin': findByMin,
		'findMin': findByMin,
		'extract': extract
	};
}(this));
